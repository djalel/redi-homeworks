import pytest
import numpy as np
from ..exercise_1 import answer_1

def test_():
    answer = answer_1()
    assert isinstance(answer, np.ndarray)
    assert np.array_equal(answer, np.array([ 0, -1,  2, -1,  4, -1,  6, -1,  8, -1]))
