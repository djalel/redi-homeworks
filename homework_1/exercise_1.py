import numpy as np

# Question 1
# Replace all odd numbers in arr with -1
def answer_1():
    arr = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    # your answer goes here
    # ...
    return arr
